<?php

namespace RachidLaasri\LaravelInstaller\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use RachidLaasri\LaravelInstaller\Events\EnvironmentSaved;
use RachidLaasri\LaravelInstaller\Helpers\EnvironmentManager;
use Validator;

class EnvironmentController extends Controller
{
    /**
     * @var EnvironmentManager
     */
    protected $EnvironmentManager;

    /**
     * @param EnvironmentManager $environmentManager
     */
    public function __construct(EnvironmentManager $environmentManager)
    {
        $this->EnvironmentManager = $environmentManager;
    }

    /**
     * Display the Environment menu page.
     *
     * @return \Illuminate\View\View
     */
    public function environmentMenu()
    {
        return view('vendor.installer.environment');
    }

    /**
     * Display the Environment page.
     *
     * @return \Illuminate\View\View
     */
    public function environmentWizard()
    {
        $envConfig = $this->EnvironmentManager->getEnvContent();

        return view('vendor.installer.environment-wizard', compact('envConfig'));
    }

    /**
     * Display the Environment page.
     *
     * @return \Illuminate\View\View
     */
    public function environmentClassic()
    {
        $envConfig = $this->EnvironmentManager->getEnvContent();

        return view('vendor.installer.environment-classic', compact('envConfig'));
    }

    /**
     * Processes the newly saved environment configuration (Classic).
     *
     * @param Request $input
     * @param Redirector $redirect
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveClassic(Request $input, Redirector $redirect)
    {

        $message = $this->EnvironmentManager->saveFileClassic($input);

        event(new EnvironmentSaved($input));

        // فيريفيكاسيون كود
        $itmId="24878940";
        $token = "aVH71sVL6UA91XchRumA8AHY5tahMXBp";

        $code = env('PURCHASE_CODE',false);
        if (!preg_match("/^(\w{8})-((\w{4})-){3}(\w{12})$/", $code)) {
            $code = false;
            $errors = $validator->errors()->add('purchase_code', 'Not valid purchase code');
        } else {

            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => "https://api.envato.com/v3/market/author/sale?code={$code}",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 20,

                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer {$token}",
                    "User-Agent: Verify Purchase Code"
                )
            ));
            $result = curl_exec($ch);
            if (isset($result) && isset(json_decode($result,true)['error'])) {
                $code = false;
                $errors = $validator->errors()->add('purchase_code', 'Not valid purchase code');
            }else{
                if (isset($result) && json_decode($result,true)['item']['id'] != $itmId) {
                    $code = false;
                    $errors = $validator->errors()->add('purchase_code', 'Not valid purchase code');
                }
            }
        }

        if (isset($errors) || !$code){
            return view('vendor.installer.environment-classic', compact('errors', 'envConfig'));
        }
        // فيريفيكاسيون كود

        return $redirect->route('LaravelInstaller::environmentClassic')
            ->with(['message' => $message]);
    }

    /**
     * Processes the newly saved environment configuration (Form Wizard).
     *
     * @param Request $request
     * @param Redirector $redirect
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveWizard(Request $request, Redirector $redirect)
    {
        $rules = config('installer.environment.form.rules');
        $messages = [
            'environment_custom.required_if' => trans('installer_messages.environment.wizard.form.name_required'),
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return view('vendor.installer.environment-wizard', compact('errors', 'envConfig'));
        }

        // فيريفيكاسيون كود
        $itmId="24878940";
        $token = "aVH71sVL6UA91XchRumA8AHY5tahMXBp";

        $code = trim($request->get('purchase_code'));
        if (!preg_match("/^(\w{8})-((\w{4})-){3}(\w{12})$/", $code)) {
            $code = false;
            $errors = $validator->errors()->add('purchase_code', 'Not valid purchase code');
        } else {

            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => "https://api.envato.com/v3/market/author/sale?code={$code}",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 20,

                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer {$token}",
                    "User-Agent: Verify Purchase Code"
                )
            ));
            $result = curl_exec($ch);
            if (isset($result) && isset(json_decode($result,true)['error'])) {
                $code = false;
                $errors = $validator->errors()->add('purchase_code', 'Not valid purchase code');
            }else{
                if (isset($result) && json_decode($result,true)['item']['id'] != $itmId) {
                    $code = false;
                    $errors = $validator->errors()->add('purchase_code', 'Not valid purchase code');
                }
            }
        }

        if (isset($errors) || !$code){
            return view('vendor.installer.environment-wizard', compact('errors', 'envConfig'));
        }
        // فيريفيكاسيون كود

        $results = $this->EnvironmentManager->saveFileWizard($request);

        event(new EnvironmentSaved($request));

        return $redirect->route('LaravelInstaller::database')
            ->with(['results' => $results]);
    }
}
